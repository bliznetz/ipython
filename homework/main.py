
import argparse


if __package__:
    from .hw1 import *
    from .hw2 import *
else:
    from hw1 import *
    from hw2 import *


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--get', '-g')
    parser.add_argument('--view', '-v')
    return parser

def open_gui():
    s = my_gui()

def main():
    parser = create_parser()
    namespace = parser.parse_args()

    #    print (namespace)

    if not (namespace.get or namespace.view):
        open_gui()
    elif namespace.get:
        url = namespace.get
        url = check_alias(url)
        print_tags_table(parce_url(url))

    else:
        url = namespace.view
        url = check_alias(url)
        print_tags_table(get_url_from_database(url))


if __name__ == '__main__':
    if __name__ == '__main__':
        main()
