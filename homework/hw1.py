import requests
from lxml import html
from collections import Counter
import sqlite3
import pickle as pickle
import yaml
# pip3 install pyyaml
import operator
from datetime import datetime

if __package__:
    from .cfg import *
else:
    from cfg import *




def tags_from_url(url):
    """ Gets, parse and count HTML tags from url.
    returns dictionary with tag-count pairs"""

    page = requests.get(url)
    tree = html.fromstring(page.content)

    all_elms = tree.cssselect('*')
    all_tags = [x.tag for x in all_elms]

    c = Counter(all_tags)
    tagsFromURL = {e: c[e] for e in c}
    return tagsFromURL


def create_database():
    """Creates SQLite database """

    conn = sqlite3.connect(DATABASE_FILE)
    with conn:
        conn.execute("""CREATE TABLE IF NOT EXISTS url (
        url TEXT NOT NULL,
        check_date TEXT NOT NULL,
        tags TEXT ) """)
    conn.close()


def parce_url(url):
    """Parses tags from URL and save to SQLite database
    Returns TAGS dictionary """

    create_database()
    conn = sqlite3.connect(DATABASE_FILE)
    parced_url = tags_from_url(url)
    with conn:
        tag_data = pickle.dumps(parced_url, 0)
        tag_data = tag_data.decode('ascii')
        sqlstring = "INSERT INTO url (rowid,url,check_date,tags) VALUES (NULL,'{}',datetime('now'),'{}')".format(url, tag_data)
        conn.execute(sqlstring)
    conn.close()
    with open("logfile.txt","a") as logfile:
        data_time = datetime.now()
        logfile.write("{} {} {}\n".format(data_time.date(), data_time.time(), url))

    return parced_url



def get_url_from_database(url):
    """Fetch tags information from database
    returns result of the last check
    returns TAGS dictionary or
    returns None if record not found"""

    conn = sqlite3.connect(DATABASE_FILE)
    with conn:
        sqlstring = "SELECT rowid,* FROM url WHERE url = '{}' ORDER BY rowid DESC".format(url)
        cursor = conn.execute(sqlstring)
        tag_data = cursor.fetchone()
        if tag_data:
            tag_data = tag_data[3]
            tag_data = tag_data.encode('ascii')
            tag_data = pickle.loads(tag_data)
        else:
            return None
    conn.close()
    return tag_data



def check_alias(url):
    """checks alias in the ALIAS_FILE, returns
    alias if it exists in the file or
    its parameter if not
    """
    try:
        with open(ALIAS_FILE) as f:
            url_map = yaml.safe_load(f)
            if url_map[url]:
                return url_map[url]
            else:
                return url
    except(FileNotFoundError, FileExistsError):
        print("File {} not found".format(ALIAS_FILE))
        return url
    except(KeyError):
        return url






def print_tags_table(tags: dict):
    """
    prints tags dictionary in the table format
    :type tags: dict
    """
    print("|============|============|")
    print("|     tag    |   amount   |")
    print("|------------|------------|")
    for i in sorted(tags.items(), key=operator.itemgetter(1), reverse=True):
        print("| {0[0]:>10} | {0[1]:<10} | ".format(i))
    print("|------------|------------|")


