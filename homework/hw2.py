from tkinter import *
from tkinter.ttk import *

import yaml

if __package__:
    from .hw1 import *
    from .cfg import *
else:
    from hw1 import *
    from cfg import *



class my_gui:

    def __init__(self):
        """Constructe form and run main loop"""
        root = Tk()
        root.title('Tag counter')

        label = Label(text = "Please choose or enter URL ")
        label.grid(row=0,column=1, columnspan = 2)

        self.combo = Combobox(values=self.get_urls_list(),width=25)
        self.combo.grid(row=1,column=1, columnspan = 2)

        self.enter = Entry(width=27)
        self.enter.grid(row=2,column=1, columnspan = 2)

        button_get = Button(root, text="Get from Internet", command=self.button_get_clicked)
        button_get.grid(row=4,column=1)

        button_view = Button(root, text="View from database", command=self.button_view_clicked)
        button_view.grid(row=4,column=2)

        self.text = Text(wrap=NONE,width=40 )
        vscrollbar = Scrollbar(orient='vert', command=self.text.yview)
        self.text['yscrollcommand'] = vscrollbar.set

        # размещаем виджеты
        self.text.grid(row=7, column=1,columnspan = 2, sticky='ns')
        vscrollbar.grid(row=7, column=3, sticky='ns')

        # конфигурируем упаковщик, чтобы текстовый виджет расширялся
        root.rowconfigure(7, weight=1)
        root.columnconfigure(1, weight=1)
        root.columnconfigure(2, weight=1)

        self.stslabel = Label(root, anchor=W, text="Waiting for user input", foreground='blue', font='arial 14')
        self.stslabel.grid(row=8, column=1, columnspan = 2)
        root.mainloop()

    def get_urls_list(self):
        """Gets URLs from config file"""
        try:
            with open(ALIAS_FILE) as f:
                url_map = yaml.safe_load(f)
                url_map = list(url_map.values())

        except(FileNotFoundError, FileExistsError):
            print("File {} not found".format(ALIAS_FILE))
            return None

        return url_map

    def button_get_clicked(self):
        """Retrive information from combo box
        and text box, call parce_url procedure
        and shows result"""
        self.stslabel.config(text="Working")

        url = self.combo.get()

        if not url:
            url = self.enter.get()
        if url:
            try:
                tags = parce_url(url)
                self.stslabel.config(text="Showing results {}".format(url))
                self.print_results(tags)
            except(requests.exceptions.MissingSchema):
                self.stslabel.config(text="Error in getting")
        else:
            self.stslabel.config(text="Please choose or enter URL")
            self.text.delete('1.0', END)


    def print_results(self, tags):
        """ Formates results into """
        self.text.delete('1.0', END)

        self.text.insert(END, "|============|============|\n")
        self.text.insert(END, "|     tag    |   amount   |\n")
        self.text.insert(END, "|------------|------------|\n")
        for i in sorted(tags.items(), key=operator.itemgetter(1), reverse=True):
            self.text.insert(END, "| {0[0]:>10} | {0[1]:<10} | \n".format(i))
        self.text.insert(END, "|------------|------------|")


    def button_view_clicked(self):
        """Retrive information from combo box
                and text box, call get_url_from_database procedure
                and shows result"""
        self.stslabel.config(text="Working")

        url = self.combo.get()

        if not url:
            url = self.enter.get()
        if url:
            tags = get_url_from_database(url)
            if tags:
                self.stslabel.config(text="Showing results {}".format(url))
                self.print_results(tags)
            else:
                self.stslabel.config(text="Url {} does not found in the database".format(url))
        else:
            self.stslabel.config(text="Please choose or enter URL")
            self.text.delete('1.0', END)

