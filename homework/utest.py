import unittest
from .hw1 import *


class tag_couner_test(unittest.TestCase):
    def test_epam(self):
        url="http://epam.com"
        inet_result=parce_url(url)
        db_result=get_url_from_database(url)
        self.assertEqual(inet_result,db_result)

    def test_alias(self):
        self.assertEqual(check_alias("epm"),"http://epam.com")

    def test_no_alias(self):
        self.assertEqual(check_alias("epm123"), "epm123")

if __name__ == '__main__':
    unittest.main()

